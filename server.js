'use strict';

const config = require('./config');

const express = require('express');
const multer = require('multer')
const DB = require('./modules/database');

const app = express();

app.set('view engine', 'pug');

// handle multipart/form-data
const upload = multer();

// serve files
app.use(express.static('public'));

// connect to DB
const dbPromise = new Promise(
    (resolve, reject) => {
        DB.connect('mongodb://'+config.user+':'+config.pwd+'@localhost/cats', resolve, reject)
    });

dbPromise.then((msg) => {
    console.log(msg);
    app.listen(3000);
}).catch((reason) => {
    console.log(reason);
});
// end connect to DB


const catSchema = {
    name: String,
    age: Number,
    gender: {
        type: 'String',
        enum: ['male', 'female']
    },
    color: String,
    weight: Number
};

const Cat = DB.getSchema('Cat', catSchema);

app.get('/cats', (req, res) => {
    Cat.find().exec().then((posts) => {
        res.send(posts);
    });
});

app.get('/cats/:name', (req, res) => {
    Cat
        .where({'name': req.params.name})
        //.where({'age': req.params.age})
        //.where({"gender": "male"})
        .exec().then((catsFound)=> {
        res.send(catsFound);
    });
});

app.post('/cats', upload.array(), (req, res) => {
    console.log(req.body);
    Cat.create(req.body).then(post => {
        res.send({status: 'OK', post: post});
    }).catch(() => {
        res.send({status: 'error', message: 'Database error'});
    });
});

app.patch('/cats', upload.array(), (req, res) => {
    console.log(req.body);
    const query = {_id: req.body.id};
    const update = {$set: req.body};

    Cat.findOneAndUpdate(query, update).then(post => {
        res.send({status: 'OK', post: post});
    }).catch(() => {
        res.send({status: 'error', message: 'Database error'});
    });
});

app.delete('/delete', upload.single('file'), (req, res) => {
    console.log(req.body);
    Spy.findByIdAndRemove(req.body.id, (err, remove) => {
        remove.save((err, deletedItem) => {
            if (err) return handleError(err);
        })
    }).then(post => {
        res.send({status: 'OK', post: post});
    }).then(() => {
        res.send({status: 'error', message: 'Database error'});
    });

});


