'use strict';

let originalData = null;
document.querySelector('#form-create').addEventListener('submit', (evt) => {
    evt.preventDefault();
    console.log(evt.target);
    const formData = new FormData(evt.target);
    const url = '/cats';
    fetch(url, {
            method: 'post',
            body: formData
        }
    ).then((resp) => {
        return resp.json();
    }).then((json) => {
        console.log(json);
        getCats();
    });
});

document.querySelector('#form-update').addEventListener('submit', (evt) => {
    evt.preventDefault();
    console.log(evt.target);
    const formData = new FormData(evt.target);
    const selectedCatIndex = document.getElementById('select-update').selectedIndex;
    const myCat = originalData[selectedCatIndex]._id;
    formData.append('id', myCat);

    const url = '/cats';
    fetch(url, {
            method: 'post',
            body: formData
        }
    ).then((resp) => {
        return resp.json();
    }).then((json) => {
        console.log(json);
        getCats();
    });
});

document.querySelector('#form-find').addEventListener('submit', (evt) => {
    evt.preventDefault();
    console.log(evt.target);
    const formData = new FormData(evt.target);
    const url = '/cats/'+formData.get('name')+'/'+formData.get('age');
    fetch(url).then((resp) => {
        return resp.json();
    })
        .then((cats) => {
            const catDiv = document.querySelector('#cats');
            catDiv.innerHTML='';
            for (const cat of cats) {
                const article = document.createElement('article');
                article.innerHTML = `
                                     <p>${cat.name}
                                     <br>${cat.gender}</p>
                                     <p>${cat.age} years</p>
                                     <p>${cat.weight} kg</p>
                                     <hr>
                                     `;
                catDiv.appendChild(article);
            }
        });
});

// delete cat
document.querySelector('#form-delete').addEventListener('submit', (evt) => {
    evt.preventDefault();
    const selectedCatIndex = document.getElementById('select-delete').selectedIndex;
    const myCat = originalData[selectedCatIndex]._id;
    const data = new FormData();
    data.append('id',myCat);

    const url = '/delete';

    fetch(url, {
        method: 'delete',
        body: data
    }).then((resp)=> {
        // console.log(resp);
        getCats();
    });
});

const getCats = () => {
    const url = '/cats';
    fetch(url)
        .then((resp) => {
            return resp.json();
        })
        .then((cats) => {
            const catDiv = document.querySelector('#cats');
            for (const cat of cats) {
                const article = document.createElement('article');
                article.innerHTML = `
                                     <p>${cat.name}
                                     <br>${cat.gender}</p>
                                     <p>${cat.age} years</p>
                                     <p>${cat.weight} kg</p>
                                     <hr>
                                     `;

                const selectOptionUpdate = document.createElement('option');
                const selectOptionDelete = document.createElement('option');
                selectOptionUpdate.value = cat.name;
                selectOptionUpdate.innerText = cat.name;
                selectOptionDelete.value = cat.name;
                selectOptionDelete.innerText = cat.name;
                catDiv.appendChild(article);
                document.querySelector('#select-update').appendChild(selectOptionUpdate);
                document.querySelector('#select-delete').appendChild(selectOptionDelete);
            }
            originalData = cats;
        })
};

getCats();